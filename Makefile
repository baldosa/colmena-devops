.PHONY: help devops

BINS_DIR = ./bin
ENV_FILE ?= .env
COMPOSE_DIR ?= devops/local/

export GREEN=\033[0;32m
export YELLOW=\033[0;33m
export NOFORMAT=\033[0m

# Add env variables if needed
ifneq (,$(wildcard ${ENV_FILE}))
	include ${ENV_FILE}
    export
endif

default: help

#❓ help: @ Displays this message
help:
	@echo ""
	@echo "List of available MAKE targets for development usage."
	@echo ""
	@echo "Usage: make [target]"
	@echo ""
	@echo "Examples:"
	@echo ""
	@echo "	make ${GREEN}build${NOFORMAT}		- Create all third party application images"
	@echo "	make ${GREEN}devops${NOFORMAT}		- Start all services"
	@echo "	make ${GREEN}setup${NOFORMAT}		- Setup applications"
	@echo ""
	@grep -E '[a-zA-Z\.\-]+:.*?@ .*$$' $(firstword $(MAKEFILE_LIST))| tr -d '#'  | awk 'BEGIN {FS = ":.*?@ "}; {printf "${GREEN}%-30s${NOFORMAT} %s\n", $$1, $$2}'
	@echo ""

#🐳 build.all:@ Stop the `docker-compose-yaml` services to build all application images
build.all: devops.down build.ldap build.nc

#🐳 build.ldap:@ Build a docker image for the ldap application
build.ldap:
	@docker image rm colmena_ldap || true
	@cd ${COMPOSE_DIR} && docker compose build openldap

#🐳 build.nc:@ Build a docker image for the nextcloud application
build.nc:
	@docker image rm colmena_nextcloud || true
	@cd ${COMPOSE_DIR} && docker compose build nextcloud

#🚀 devops:@ Start services using Tilt
devops:
	@cd ${COMPOSE_DIR} && tilt up

#⚙️  devops.up:@   Starts up the `docker-compose.yaml` services located in ./devops/local
devops.up:
	@cd ${COMPOSE_DIR} && docker compose up

#⚙️  devops.down:@   Stops the `docker-compose.yaml` services located in ./devops/local
devops.down:
	@cd ${COMPOSE_DIR} && docker compose down

#📦 setup.nc:@ Runs setup operations for nextcloud (requires nrxtcloud to be running)
setup.nc:
	@cd ${COMPOSE_DIR} && docker compose exec --user www-data nextcloud php occ app:enable user_ldap

#📦 setup:@ Runs setup tasks for all applications
setup: setup.nc

#💻 connect.dev: @ Start dev a tmux session
connect.dev: SESSION:=dev
connect.dev: DEV_USER:=${DEV_USER}
connect.dev: DEV_HOST:=${DEV_HOST}
connect.dev:
	@${BINS_DIR}/tmux.sh ${SESSION}
