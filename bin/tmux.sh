#!/bin/bash

# ------------------------------------------------------------------------------
# Globals
#

# ------------------------------------------------------------------------------
# Sessions
#

dev() {
  ssh ${DEV_USER}@${DEV_HOST}
}

# ------------------------------------------------------------------------------
# Commands
#

case $1 in
  dev) "$@"; exit;;
esac