# Nextcloud

## Getting started

* Check you can login to thehe nextcloud local server that is located at `https://localhost:8003`.

## LDAP Configuration

* Open the browser at `http://localhost:8003/settings/admin/ldap`

### Server configuration

* Configure the USER DN to use `admin` as the cn
* Use the value configured in `LDAP_ADMIN_PASSWORD` as password
* The base DN should be detected automatically

### User configuration

Use the following query

> Though it's not safe we're using the `*` wildcard.

```ldap
(|(objectclass=*))
```

### Login attributes

Check the box that says `LDAP/AD Email Address`. Then use the following query:

```ldap
(&(|(objectclass=*))(|(|(mailPrimaryAddress=%uid)(mail=%uid))(|(objectClass=%uid))))
```

### Groups

Use the following query:

```ldap
(&(|(objectclass=organizationalUnit)))
```

### Advanced settings

In the advanced settings tab set the following values:

* Base User Tree: update the value so to `ou=users,<dn...>`
* Base Group Tree: update the value so to `ou=teams,<dn...>`
