# Colmena Devops

Thie repository provides all developer operations setup related to third party applications for the Colmena environment. The idea is to centralize and develop an efficient and zero-effort (or almost zero) setup configuration for developers and devops engineers.

Changes to this repository should consider scaling up devops and environment configurations from local machines to remote machines. For example: we might want to run CI triggers from this repository to easily deploy a nextcloud, ldap or any other application. Templates, charts, compose or whatever file we need for our deployment strategy should be versioned under the devops directory, probably with a subfolder describing the environment name (e.g.: `dev`).

## Requirements

> Note that this has been defined to use the latest versions but might work with not so old versions too. Take this versions as the upmost and desirable requirement.

* Docker: [`24.0.2`](https://docs.docker.com/desktop/install/ubuntu/)
* Compose plugin: [`v2.3.3`](https://docs.docker.com/compose/install/linux/)
* Tilt: [`0.32.4`](https://docs.tilt.dev/#macoslinux) (Optional)

## Getting started

### Local environmemt

Make a copy of the base env file

> At the moment there's no need to update any value, since we're not storing or using any valuable secert for local development environments.

```bash
cp .env.example .env
```

We use `make` to quickly run predefined tasks to setup, start and stop our environment. 

```bash
# Type make to display the default help message.
# You'll notice a bunch of suggestions, skip them for now, they'll be useful as
# a reminder.
make
```

The first time we clone this (or every time the Dockerfile of an image is updated) we need to build application images.

```bash
make build.all
```

If you're editing for example the nextcloud Dockerfile you would only build nextcloud by doing:

```bash
make build.nc
```

Now you can start all services:

```bash
make devops
```

This will prompt a few options in your terminal, press T to navgate through services or Spacebar to open a friendly user interface. There you'll be able to get every app status and search for logs. If you did not install `tilt` then type `make devops.up`, it will start services with `docker compose`. You can then put them down with `make devops.down`.

If you're running this for the first time you'll notice the nextcloud application takes around a minute to finish it's installation. When the installation finishes you'll be able to execute the next command from another terminal.

This will setup third party applications, but mainly it will enable ldap integration in Nextcloud. This is a one-time comand unless more application setup instructions are added during the development process.

```bash
make setup
```

You should be prompted with this message:

```bash
user_ldap 1.15.0 enabled
```

That's all. The next time you need to start services just run:

```bash
make devops
```

#### Postgres Admin

The local development environment uses a postgres database. We use the PGAdmin app to debug registries, create queries or just inspect information from the database.

See instructions at the [apps/pgadmin](./devops/apps/pgadmin/README.md) section.

#### LDAP Admin

See instructions at the [devops/apps/ldap_admin](./devops/apps/ldap_admin/README.md) section.

### Dev Environment

> TODO

### Stage Environemnt

> TODO
